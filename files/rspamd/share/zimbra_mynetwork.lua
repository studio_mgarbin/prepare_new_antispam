local lualdap = require "lualdap"
local lua_redis = require 'lua_redis'
local lua_util = require "lua_util"
local rspamd_logger = require "rspamd_logger"
local options = dofile("/etc/rspamd/zimbra.cfg.lua")

local N = "zimbra_mynetwork"
local redis_params

-- function for search in ldap
local function ldap_search(task,type)

  local ld = assert(lualdap.open_simple (options.ldap_hostname, options.ldap_bind_dn, options.ldap_bind_password, false))
  local ricerca = ""
  if ld~=nil
  then
    if type == "server" then
      -- rspamd_logger.infox(task, 'MYNETWORK from SERVER config %1 %2 %3 %4',options.ldap_hostname, options.ldap_bind_dn, options.ldap_bind_password, options.zimbra_server)
      ricerca = ld:search { filter="(&(objectClass=zimbraServer)(cn=".. options.zimbra_server .."))", sizelimit=2, base="", scope="subtree" }
    else
      -- rspamd_logger.infox(task, 'MYNETWORK from GLOBAL config %1 %2 %3 %4',options.ldap_hostname, options.ldap_bind_dn, options.ldap_bind_password, options.zimbra_server)
      ricerca = ld:search { sizelimit=2, base="cn=config,cn=zimbra", scope="base" }
    end
    if ricerca then
      for dn, attribs in ricerca do
        for name, values in pairs (attribs) do
          if name == "zimbraMtaMyNetworks" then
            -- rspamd_logger.infox(task, 'MYNETWORK VALUE %1', values)
            ld:close()
            return values
          end
        end
      end
    end
    ld:close()
  end
end

-- function to set expire on redis
local function set_expire(task,command,hash)
  local function redis_set_expire_cb(err)
    if err ~= nil then
      rspamd_logger.errx(task, 'redis_get_cb received error: %1', err)
      return
    end
  end

  local ret_set_expire = lua_redis.redis_make_request(
    task,
    redis_params, -- connect params
    hash, -- hash key
    true, -- is write
    redis_set_expire_cb, --callback
    command, -- command
    { hash, options.mynetwork_expire_cache } -- arguments
  )

  if not ret_set_expire then
    rspamd_logger.errx(task, "MYNETWORK set_expire request wasn't scheduled")
  end

end

-- function to set new cache value
local function set_cache(task,command,hash,key,value)
  local function redis_set_cache_cb(err)
    if err ~= nil then
      rspamd_logger.errx(task, 'redis_get_cb received error: %1', err)
      return
    end
  end

  local ret_set_cache = lua_redis.redis_make_request(
    task,
    redis_params, -- connect params
    hash, -- hash key
    true, -- is write
    redis_set_cache_cb, --callback
    command, -- command
    { hash, key, value } -- arguments
  )

  if not ret_set_cache then
    rspamd_logger.errx(task, "MYNETWORK set_cache request wasn't scheduled")
  end

end

-- function for check cache and do all the thing
local function check_cache(task,command,hash,key)

  local function redis_get_cb(err, data)
    if err ~= nil then
      rspamd_logger.errx(task, 'redis_get_cb received error: %1', err)
      return
    end

    --if we can't find key in table {data}, search for value in ldap and recreate it
    --rspamd_logger.infox(task, 'CACHE FOUND: %1', data )
    if next(data) == nil then
      local result=ldap_search(task,"server")
      if result == "" or result == nil then
        -- if no server config pass to global
        result=ldap_search(task,"global")
      end
      local ip = ""
      local ret_set_cache
      -- set new value in redis
      for ip in result:gmatch("%S+") do
        ret_set_cache=set_cache(task,"HSET",options.zimbra_redis_key,ip,ip)
        -- rspamd_logger.infox(task, 'GET MYNETWORK FROM ZIMBRA LDAP %1',ip)
      end
      -- set expire
      ret_set_expire=set_expire(task,"EXPIRE",options.zimbra_redis_key)
    end
  end

  local ret_check_cache = lua_redis.redis_make_request(
    task,
    redis_params, -- connect params
    hash, -- hash key
    false, -- is write
    redis_get_cb, --callback
    command, -- command
    { hash } -- arguments
  )

  if not ret_check_cache then
    rspamd_logger.errx(task, "MYNETWORK check_cache request wasn't scheduled")
  end

end

-- Start check if exist cache in redis, if not search in ldap then write all MtaMyNetwork
-- into redis and set an expire to the key 
function run_cb(task)
    local redis_cache_result=check_cache(task,"HGETALL",options.zimbra_redis_key)
end

local settings = {}
local opts =  rspamd_config:get_all_opt('zimbra_mynetwork')
if opts then
  for k,v in pairs(opts) do
    settings[k] = v
  end

  redis_params = lua_redis.parse_redis_server('redis')
  if not redis_params then
    rspamd_logger.infox(rspamd_config, 'no servers are specified, disabling module')
    lua_util.disable_module(N, "redis")
  else
    rspamd_config:register_symbol({
      name = "ZIMBRA_MYNET",
      flags = "nostat", -- `nice` if symbol can produce negative score;`empty` if symbol can be called for empty messages;`skip` if symbol should be skipped now;`nostat` if symbol should be excluded from stat tokens;`trivial` symbol is trivial (e.g. no network requests)
      type = 'prefilter', -- super first
      callback = run_cb, -- callback
      priority = 1 -- priority first of whitelist mapping  
    })
  end
end
