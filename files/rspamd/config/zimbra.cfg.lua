local options = {
    redis_server="REDIS_SERVER",
    zimbra_server = "ZIMBRA_SERVER",
    zimbra_redis_key = "mynetwork_zimbra",
    mynetwork_expire_cache = 600,
    ldap_hostname      = 'LDAP_HOSTNAME',
    ldap_bind_dn       = 'uid=zimbra,cn=admins,cn=zimbra',
    ldap_bind_password = 'LDAP_PASSWORD',
}
return options
