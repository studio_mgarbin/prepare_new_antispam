local logger = require "rspamd_logger"
local lualdap = require "lualdap"
local rspamd_redis = require "rspamd_redis"
local options2 = dofile("/etc/rspamd/zimbra.cfg.lua")
local rspamd_regexp = require "rspamd_regexp"


local function user_check_bwlist(user,bw_type,task)

    local ld = assert(lualdap.open_simple (options2.ldap_hostname, options2.ldap_bind_dn, options2.ldap_bind_password, false))

    local result = {}
    local search = ""
    local find = 0

    if ld~=nil
    then
        if bw_type == "blacklist"
        then
            search="amavisBlacklistSender"
        else
            search="amavisWhitelistSender"
        end

        local ricerca = ld:search { filter="(&(zimbraMailDeliveryAddress=".. user ..")(".. search .."=*)(zimbraMailStatus=enabled))", sizelimit=2, base="", scope="subtree" }
        if ricerca
        then
            for dn, attribs in ricerca do
                for name, values in pairs (attribs) do
                    if name == search then
                        -- logger.infox(task, 'BWL type %1',tostring(type(values)))
                        if type(values) == "table"
                        then
                            for z=tonumber(#values),1,-1
                            do
                                result[z] = string.lower(string.gsub(values[z], "<(.-)>","%1"))
                            end
                            ld:close()
                            return result
                        else
                            result[1] = string.lower(string.gsub(values, "<(.-)>","%1"))
                            ld:close()
                            return result
                        end
                    end
                end
            end
        end
        ld:close()
    end
end

local function check_user_bwlist(task)

    local smtp_recipients_to = task:get_recipients(1)

    local from = ''
    local black_found = 0
    local white_found = 0

    local header_from = ''
    local header_from_domain = ''
    local black_header_found = 0
    local white_header_found = 0

    if (smtp_recipients_to ~= nil and smtp_recipients_to ~= "")
    then

        if task:has_from('smtp') then
            from = task:get_from('smtp')[1]
        end

        if task:has_from('mime') then
            local temp_from = task:get_from('mime')[1]
            header_from  = string.lower(temp_from['addr'])
            header_from_domain = string.lower(temp_from['domain'])
            -- logger.infox(task, 'TEST bwlist RECIPIENT %1 DOMAIN %2', header_from , header_from_domain)
        end

        -- logger.infox(task, 'bwlist %1 %2',from,smtp_recipients_to)
        for i=tonumber(#smtp_recipients_to),1,-1
        do
            -- logger.infox(task, 'BWL %1 %2',from,smtp_recipients_to[i])

            local real_email=smtp_recipients_to[i]['addr']

            local get_blacklist=user_check_bwlist(real_email,"blacklist",task)

            if get_blacklist ~= nil and get_blacklist ~= "" and tonumber(#get_blacklist) > 0
            then
                -- logger.infox(task, 'BWL BLACKLIST %1 %2',#get_blacklist,get_blacklist)
                for z=tonumber(#get_blacklist),1,-1
                do
                    if from ~= "" and ( string.lower(from['addr']) == get_blacklist[z] or string.lower(from['domain']) == get_blacklist[z] )
                    then
                        logger.infox(task, 'BWL - user %1 has blacklisted %2 with word search "%3", add positive score 100.0 (reject)',real_email,from['addr'],get_blacklist[z])
                        black_found=black_found+1
                        break
                    end

                    if header_from == get_blacklist[z] or header_from_domain == get_blacklist[z]
                    then
                        logger.infox(task, 'BWL HEADER FROM - user %1 has blacklisted %2 with word search "%3", add positive score 10.0',real_email,header_from,get_blacklist[z])
                        black_header_found=black_header_found+1
                        break
                    end
                end
            end


            if black_found > 0
            then
                task:insert_result('USER_BLACKLIST', 0.0)
                task:adjust_result('USER_BLACKLIST', tonumber("100.0"))
                return true
            elseif black_header_found > 0
            then
                task:insert_result('USER_BLACKLIST', 0.0)
                task:adjust_result('USER_BLACKLIST', tonumber("10.0"))
                return true
            end

            local get_whitelist=user_check_bwlist(real_email,"whitelist",task)

            if get_whitelist ~= nil and get_whitelist ~= "" and tonumber(#get_whitelist) > 0
            then
                -- logger.infox(task, 'BWL WHITELIST %1 %2',#get_whitelist,get_whitelist)
                for z=tonumber(#get_whitelist),1,-1
                do
                    if from ~= "" and ( string.lower(from['addr']) == get_whitelist[z] or string.lower(from['domain']) == get_whitelist[z] )
                    then
                        logger.infox(task, 'BWL - user %1 has whitelisted %2 with word search "%3", add negative score -100.0',real_email,from['addr'],get_whitelist[z])
                        white_found=white_found+1
                        break
                    end

                    if header_from == get_whitelist[z] or header_from_domain == get_whitelist[z]
                    then
                        logger.infox(task, 'BWL HEADER FROM - user %1 has whitelisted %2 with word search "%3", add negative score -10.0',real_email,header_from,get_whitelist[z])
                        white_header_found=white_header_found+1
                        break
                    end
                end
            end
        -- end for 
        end

        if white_found > 0
        then
            task:insert_result('USER_WHITELIST', 0.0)
            task:adjust_result('USER_WHITELIST', tonumber("-100.0"))
            return true
        elseif white_header_found > 0
        then
            task:insert_result('USER_WHITELIST', 0.0)
            task:adjust_result('USER_WHITELIST', tonumber("-10.0"))
            return true
        end
    end

end

local function domain_check_bwlist(domain,bw_type,task)

    local ld = assert(lualdap.open_simple (options2.ldap_hostname, options2.ldap_bind_dn, options2.ldap_bind_password, false))

    local result = {}
    local search = ""
    local find = 0

    if ld~=nil
    then
        if bw_type == "blacklist"
        then
            search="amavisBlacklistSender"
        else
            search="amavisWhitelistSender"
        end
        local ricerca = ld:search { filter="(&(objectClass=zimbraDomain)(zimbraDomainName=".. domain ..")(".. search .."=*)(zimbraDomainStatus=active))", sizelimit=2, base="", scope="subtree" }
        if ricerca
        then
            for dn, attribs in ricerca do
                for name, values in pairs (attribs) do
                    if name == search then
                        if type(values) == "table"
                        then
                            for z=tonumber(#values),1,-1
                            do
                                result[z] = string.lower(string.gsub(values[z], "<(.-)>","%1"))
                            end
                            ld:close()
                            return result
                        else
                            result[1] = string.lower(string.gsub(values, "<(.-)>","%1"))
                            ld:close()
                            return result
                        end
                    end
                end
            end
        end
        ld:close()
    end
end


local function check_domain_bwlist(task)

    local smtp_recipients_to = task:get_recipients(1)
    local unique_domain = {}

    local from = ''
    local black_found = 0
    local white_found = 0

    local header_from = ''
    local header_from_domain = ''
    local black_header_found = 0
    local white_header_found = 0

    if (smtp_recipients_to ~= nil and smtp_recipients_to ~= "")
    then

        if task:has_from('smtp') then
            from = task:get_from('smtp')[1]
        end

        if task:has_from('mime') then
            local temp_from = task:get_from('mime')[1]
            header_from  = string.lower(temp_from['addr'])
            header_from_domain = string.lower(temp_from['domain'])
            -- logger.infox(task, 'TEST bwlist RECIPIENT %1 DOMAIN %2', header_from , header_from_domain)
        end

        -- deduplicate recipient domain
        for i=tonumber(#smtp_recipients_to),1,-1
        do
            local domain = smtp_recipients_to[i]['domain']
            if not unique_domain[domain] then

                unique_domain[domain] = true
                local real_email=smtp_recipients_to[i]['addr']

                local get_blacklist=domain_check_bwlist(domain,"blacklist",task)

                if get_blacklist ~= nil and get_blacklist ~= "" and tonumber(#get_blacklist) > 0
                then
                    for z=tonumber(#get_blacklist),1,-1
                    do
                        if from ~= "" and ( string.lower(from['addr']) == get_blacklist[z] or string.lower(from['domain']) == get_blacklist[z])
                        then
                            logger.infox(task, 'BWL DOMAIN - domain %1 has blacklisted %2 with word search "%3", add positive score 30.0 (reject)',real_email,from['addr'],get_blacklist[z])
                            black_found=black_found+1
                            break
                        end
                        if header_from == get_blacklist[z] or header_from_domain == get_blacklist[z]
                        then
                            logger.infox(task, 'BWL DOMAIN HEADER FROM - user %1 has blacklisted %2 with word search "%3", add positive score 5.0',real_email,header_from,get_blacklist[z])
                            black_header_found=black_header_found+1
                            break
                        end
                    end
                end

                if black_found > 0
                then
                    task:insert_result('DOMAIN_BLACKLIST', 0.0)
                    task:adjust_result('DOMAIN_BLACKLIST', tonumber("50.0"))
                    return true
                elseif black_header_found > 0
                then
                    task:insert_result('DOMAIN_BLACKLIST', 0.0)
                    task:adjust_result('DOMAIN_BLACKLIST', tonumber("10.0"))
                    return true
                end

                local get_whitelist=domain_check_bwlist(domain,"whitelist",task)

                if get_whitelist ~= nil and get_whitelist ~= "" and tonumber(#get_whitelist) > 0
                then
                    for z=tonumber(#get_whitelist),1,-1
                    do
                        if from ~= "" and ( string.lower(from['addr']) == get_whitelist[z] or string.lower(from['domain']) == get_whitelist[z] )
                        then
                            logger.infox(task, 'BWL DOMAIN - domain %1 has whitelisted %2 with word search "%3", add negative score -30.0',real_email,from['addr'],get_whitelist[z])
                            white_found=white_found+1
                            break
                        end
                        if header_from == get_whitelist[z] or header_from_domain == get_whitelist[z]
                        then
                            logger.infox(task, 'BWL DOMAIN HEADER FROM - user %1 has whitelisted %2 with word search "%3", add negative score -5.0',real_email,header_from,get_whitelist[z])
                            white_header_found=white_header_found+1
                            break
                        end
                    end
                end

                if white_found > 0
                then
                    task:insert_result('DOMAIN_WHITELIST', 0.0)
                    task:adjust_result('DOMAIN_WHITELIST', tonumber("-50.0"))
                    return true
                elseif white_header_found > 0
                then
                    task:insert_result('DOMAIN_WHITELIST', 0.0)
                    task:adjust_result('DOMAIN_WHITELIST', tonumber("-10.0"))
                    return true
                end

            end
        end
    end
end

rspamd_config.USER_BWLIST = {
  callback = function(task)
    return check_user_bwlist(task)
  end,
  description = 'White/blacklist per user'
}


rspamd_config.DOMAIN_BWLIST = {
  callback = function(task)
    return check_domain_bwlist(task)
  end,
  description = 'White/blacklist per domain'
}


local function check_fake_url(task)

    local body = task:get_text_parts()
    local urls = task:get_urls()
    local isITA = false

    if body ~= nil and urls ~= nil
    then
        local country = task:get_mempool():get_variable('country')
        if country then
            if country == "IT" then
                isITA = true
            end
        end
        local onedrive_re = rspamd_regexp.create('/https:\\/\\/(onedrive\\.live\\.com\\/download)/i')
        local google_re = rspamd_regexp.create('/https:\\/\\/(www\\.google\\.com\\/url\\?)/i')
        local pwd_re = rspamd_regexp.create('/(password|parola|numero|zip).{0,5}(?:)/i')

        for _,p in ipairs(task:get_text_parts()) do
            local result1 = onedrive_re:search(p:get_content_oneline(),false,true)
            local result2 = pwd_re:search(p:get_content_oneline(),false,true)
            if result1 and result2 then
                if country then
                    logger.infox(task, '[URL_FAKE_PHISH] COUNTRY -> %1', country)
                end
                logger.infox(task, '[URL_FAKE_PHISH] FAKE URL WITH PASSWORD %1', result1)
                task:insert_result('SSTORTI_SUSPICIOUS_ONEDRIVE_LINK', 0.0)
                task:adjust_result('SSTORTI_SUSPICIOUS_ONEDRIVE_LINK', tonumber("9.0"))
            elseif result1 and not isITA then
                if country then
                    logger.infox(task, '[URL_FAKE_PHISH] COUNTRY -> %1', country)
                end
                logger.infox(task, '[URL_FAKE_PHISH] FAKE URL WITHOUT PASSWORD %1', result1)
            end
        end
    end
end

local function check_attach_pwd(task)

    local sfile = {}
    local ret = {}
    local mfile = task:get_parts()
    local body = task:get_text_parts()
    local isITA = false
    local numFile = 0
    local isPec = false
    local verify_ext = rspamd_regexp.create('/(doc|docx|xls|xlm|pptx|zip|rar)/i')

    if tonumber(#mfile) > 0
    then
        for _,tsfile in ipairs(mfile) do
            local tfname = tsfile:get_filename()
            if tfname ~= nil and tsfile:is_attachment()
            then
                local exten = string.match(tfname, '([^.]+)$')
                if verify_ext:search(exten,false,true) then
                    numFile = numFile + 1
                end
                if tfname == "daticert.xml" then
                    isPec = true
                end
            end
        end
    end


    --logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] number attachment %1', numFile)

    if mfile ~= nil and body ~= nil and numFile == 1 
    then

        local country = task:get_mempool():get_variable('country')
        if country then
            if country == "IT" then
                isITA = true
            end
        end

        for _,sfile in ipairs(mfile) do
            local fname = sfile:get_filename()
            local isencrypt = false
            local have_archive = false
            if fname ~= nil and sfile:is_attachment()
            then
                if sfile:is_archive()
                then
                    have_archive = true
                    local arch = sfile:get_archive()
                    --logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] ARCHIVE FOUND')
                    if arch:is_encrypted()
                    then
                        logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] ARCHIVE ENCRYPTED')
                        isencrypt = true
                    elseif arch:is_unreadable()
                    then
                        logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] ARCHIVE UNREADABLE')
                    --elseif arch:is_obfuscated()
                    --then
                    --    logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] -> ARCHIVE OBFUSCATED')
                    --else
                    --    local arch_type = arch:get_type()
                    --    if arch_type == 'zip' then
                    --        local archfiles = arch:get_files(2) or {}
                    --        if tonumber(#archfiles) < 2 then
                    --            for _,archfile in ipairs(archfiles) do
                    --                logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] -> ARCHIVE FILE : %1',archfile)
                    --            end
                    --        end
                     --   end
                    end
                end
                local fname_no_ext = string.gsub(fname, '%.([^.]+)$',"")
                --local fname_no_ext = fname:sub(1, -2)
                --logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] solo nome -> %1',fname_no_ext)
                local fname_re
                local pwd_re
                if fname_no_ext ~= nil
                then
                    fname_re = rspamd_regexp.create('/' .. fname_no_ext .. '\\.(?=[^.]+)(?=(doc|docx|xls|xlm|pptx|zip|rar))/i')
                else
                    fname_re = rspamd_regexp.create('/' .. fname .. '/i')
                end

                -- password word
                local pwd_re = rspamd_regexp.create('/(password|parola|numero|zip).{0,5}(?:)/i')

                if body ~= nil
                then
                    for _,p in ipairs(task:get_text_parts()) do
                        --logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] -> %1', p:get_content_oneline())
                        local result1 = fname_re:search(p:get_content_oneline(),false,true)
                        local result2 = pwd_re:search(p:get_content_oneline(),false,true)
                        if result1 and result2 and have_archive
                        then
                            if country then
                                logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] COUNTRY -> %1', country)
                            end
                            logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] - ARCHIVE - Found same attachment name "%1" in text body', fname)
                            task:insert_result('SSTORTI_SUSPICIOUS_ZIP_ATTACHMENT', 0.0)
                            if isPec then
                              task:adjust_result('SSTORTI_SUSPICIOUS_ZIP_ATTACHMENT', tonumber("1.5"))
                            else
                              task:adjust_result('SSTORTI_SUSPICIOUS_ZIP_ATTACHMENT', tonumber("9.0"))
                            end
                        elseif result1 then
                            if country then        
                                logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] COUNTRY -> %1', country)
                            end
                            logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] - DOC - Found same attachment name "%1" in text body', fname)
                            task:insert_result('SSTORTI_SUSPICIOUS_DOC_ATTACHMENT', 0.0)
                            if isPec then
                                task:adjust_result('SSTORTI_SUSPICIOUS_DOC_ATTACHMENT', tonumber("1.0"))
                            else
                                task:adjust_result('SSTORTI_SUSPICIOUS_DOC_ATTACHMENT', tonumber("9.0"))
                            end
                        end
                    end
                end

               --logger.infox(task, '[FILE_NAME_ATTACHMENT_IN_BODY] %1',fname)
            end
        end
    end

    return false

end

rspamd_config.CK_AT_PWD = {
    description = "Check if attachment name is inside body and if there is a password",
    callback = function(task)
      return check_attach_pwd(task)
    end
}

rspamd_config.CK_FAKE_URL = {
    description = "Check fake onedrive and google redirect url",
    callback = function(task)
      return check_fake_url(task)
    end
}
