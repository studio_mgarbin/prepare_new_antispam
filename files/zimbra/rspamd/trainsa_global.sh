#!/bin/bash
 #
 # ***** BEGIN LICENSE BLOCK *****
 # Zimbra Collaboration Suite Server
 # Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010, 2012, 2013, 2014, 2015, 2016 Synacor, Inc.
 #
 # This program is free software: you can redistribute it and/or modify it under
 # the terms of the GNU General Public License as published by the Free Software Foundation,
 # version 2 of the License.
 #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 # without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 # See the GNU General Public License for more details.
 # You should have received a copy of the GNU General Public License along with this program.
 # If not, see < gnu dot org /licenses/>.
 # ***** END LICENSE BLOCK *****
 #
 
 # This section trains the system ham/spam accounts
 #
 autoTrainSystem() {   
 
 # This is the section for extracting the email to a
 # couple of temp directories for spam & ham
  timestampit "Starting spam/ham extraction from system accounts."
  spamdir=`mktemp -d -t spam.XXXXXXX` || exit 1
  hamdir=`mktemp -d -t ham.XXXXXXX` || exit 1
  /opt/zimbra/libexec/zmspamextract ${spam_account} -o ${spamdir}
  /opt/zimbra/libexec/zmspamextract ${ham_account} -o ${hamdir}
  timestampit "Finished extracting spam/ham from system accounts."
 
 # This is the actual section for rspamd training
  timestampit "Starting rspamd system accounts training."
 
 # Let's do a test here to see if rspamc is doing it's thing!
 # List some stats before training
 # the passwords for these rspamc commands need to be changed for your server
 
   timestampit "List rspam stats before training."
   /usr/bin/rspamc -h 127.0.0.1:11334 stat
 
 # do the spam directory
    grep --files-with-matches  --recursive -v -e "\"Antispam Filter\"" -e "disposition-notification" ${spamdir} |
    while read file ; do  if [[ $(grep -i "^Message-ID:" $file|wc -l) == 1 ]] ;then echo "$file" ; fi ; done|
    xargs --no-run-if-empty --max-args=3 bash -c '/usr/bin/rspamc -c bayes_global -h 127.0.0.1:11334 learn_spam $1'
 
 # do the ham directory
    grep --files-with-matches  --recursive -v -e "\"Antispam Filter\"" -e "disposition-notification" ${hamdir} |
    while read file ; do  if [[ $(grep -i "^Message-ID:" $file|wc -l) == 1 ]] ;then echo "$file" ; fi ; done|
    xargs --no-run-if-empty --max-args=3 bash -c '/usr/bin/rspamc -c bayes_global -h 127.0.0.1:11334 learn_spam $1' 

 # List some stats after training
   timestampit "List rspam stats after training."
   /usr/bin/rspamc -h 127.0.0.1:11334 stat
   timestampit "Finished rspamd training."
 # End of the rspamd training section for system ham/spam accounts
 
  /bin/rm -rf ${spamdir} ${hamdir}
 
 }
 
 # The following is the section that trains rspamd for the user $FOLDER (ham or spam)
 #
 trainAccountFolder() {
 
 timestampit  "Starting rspamd user accounts training"
  tempdir=`mktmpdir ${MODE}`
  if [ "x${MODE}" = "xspam" ]; then
    FOLDER=${FOLDER:=junk}
  elif [ "x${MODE}" = "xham" ]; then
    FOLDER=${FOLDER:=inbox}
  fi
 
 # extract the user ham/spam and train rspamd
 timestampit  "Starting rspamd $MODE training for $USER using folder $FOLDER"
  /opt/zimbra/libexec/zmspamextract -r -m $USER -o ${tempdir} -q in:${FOLDER}
   
  if [ "x${MODE}" = "xspam" ]; then
    echo "sono qui"
    /usr/bin/rspamc -c bayes_user -h 127.0.0.1:11334 learn_spam ${tempdir} || exit 1
    FOLDER=${FOLDER:=junk}
  elif [ "x${MODE}" = "xham" ]; then
    /usr/bin/rspamc -c bayes_user -h 127.0.0.1:11334 learn_ham ${tempdir} || exit 1
    FOLDER=${FOLDER:=inbox}
   fi
 timestampit  "Finished rspamd $MODE training for $USER using folder $FOLDER"
 
  /bin/rm -rf ${tempdir}
 
 }

 mktmpdir() {
  mktemp -d "${zmtrainsa_tmp_directory:-${zimbra_tmp_directory}}/rspamd.$$.$1.XXXXXX" || exit 1
 }
 
 timestampit() {
   SIMPLE_DATE=`date +%Y%m%d%H%M%S`
   echo "$SIMPLE_DATE $1"
 }
 
 usage() {
   echo "Usage: $0 <user> <spam|ham> [folder]"
   exit 1
 }
 
 if [ x`whoami` != xzimbra ]; then
     echo Error: must be run as zimbra user
   exit 1
 fi
 
 source /opt/zimbra/bin/zmshutil || exit 1
 zmsetvars
 
 amavis_dspam_enabled=`/opt/zimbra/bin/zmprov -l gs ${zimbra_server_hostname} zimbraAmavisDSPAMEnabled | grep zimbraAmavisDSPAMEnabled: | awk  '{print $2}'`
 amavis_dspam_enabled=$(echo $amavis_dspam_enabled | tr A-Z a-z)
 antispam_mysql_enabled=$(echo $antispam_mysql_enabled | tr A-Z a-z)
 zmtrainsa_cleanup_host=$(echo $zmtrainsa_cleanup_host | tr A-Z a-z)
 
 if [ "x${zimbra_spam_externalIsSpamAccount}" = "x" ]; then
   spam_account="-s"
 else
   spam_account="-m ${zimbra_spam_externalIsSpamAccount}"
 fi
 
 if [ "x${zimbra_spam_externalIsNotSpamAccount}" = "x" ]; then
   ham_account="-n"
 else
   ham_account="-m ${zimbra_spam_externalIsNotSpamAccount}"
 fi
 
 # Set db_path
 if [ x"$antispam_mysql_enabled" = "xtrue" ]; then
   db_path='/opt/zimbra/data/amavisd/mysql/data'
 else
   db_path='/opt/zimbra/data/amavisd/.spamassassin'
 fi
 
 # No argument mode uses zmspamextract for auto-training.
 if [ x$1 = "x" ]; then
   autoTrainSystem
   exit
 fi
 
 if [ x$1 = "x--cleanup" ]; then
   if [ x${zmtrainsa_cleanup_host} = "xtrue" ]; then
     timestampit "Starting spam/ham cleanup"
     mydir=`mktemp -d -t cleanup.XXXXXX` || exit 1
     /opt/zimbra/libexec/zmspamextract ${spam_account} -o ${mydir} -d
     /opt/zimbra/libexec/zmspamextract ${ham_account} -o ${mydir} -d
     /bin/rm -rf ${mydir}
     timestampit "Finished spam/ham cleanup"
   else
     timestampit "Cleanup skipped: $zimbra_server_hostname is not a spam/ham cleanup host."
  fi
   exit
 fi
 
 USER=$1
 MODE=`echo $2 | tr A-Z a-z`
 FOLDER=$3
 
 if [ "x${MODE}" != "xspam" -a "x${MODE}" != "xham" ]; then
   usage
 fi
 
 if [ "x${USER}" = "x" ]; then
  usage
 fi
 
 trainAccountFolder
 
 exit 0
