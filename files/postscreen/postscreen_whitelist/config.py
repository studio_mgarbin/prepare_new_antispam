import logging

ip='127.0.0.1'
port=5555
whitelist_domain='/etc/postscreen_whitelist/domain.local'
pidfile='/var/run/postscreen_whitelist.pid'
logfile='/var/log/postscreen_whitelist.log'
loglevel=logging.INFO
