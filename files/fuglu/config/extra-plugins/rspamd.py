# -*- coding: UTF-8 -*-
from fuglu.shared import ScannerPlugin, REJECT, DELETE, DUNNO, DEFER, Suspect, string_to_actioncode, apply_template
from fuglu.extensions.sql import DBConfig
from fuglu.bounce import Bounce
from email.parser import Parser
import hashlib
import re
import json
import tempfile
import os

from BeautifulSoup import BeautifulSoup

try:
    from httplib import HTTPConnection
except ImportError:
    from http.client import HTTPConnection

GTUBE = """Date: Mon, 08 Sep 2008 17:33:54 +0200
To: oli@unittests.fuglu.org
From: oli@unittests.fuglu.org
Subject: test scanner

  XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X
"""



class RSpamdPlugin(ScannerPlugin):


    def __init__(self, config, section=None):
        ScannerPlugin.__init__(self, config, section)
        self.logger = self._logger()
        self.requiredvars = {
            'host': {
                'default': '127.0.0.1',
                'description': 'hostname where rspamd runs',
            },

            'port': {
                'default': '11333',
                'description': 'port number of rspamd TCP socket ("normal" worker)',
            },

            'timeout': {
                'default': '30',
                'description': 'how long should we wait for an answer from rspamd',
            },

            'scanoriginal': {
                'default': '1',
                'description': "should we scan the original message as retreived from postfix or scan the current state \nin fuglu (which might have been altered by previous plugins)\nonly set this to disabled if you have a custom plugin that adds special headers to the message that will be \nused in rspamd rules",
            },

            'forwardoriginal': {
                'default': '0',
                'description': """forward the original message or replace the content as returned by spamassassin\nif this is set to True/1/Yes , no spamassassin headers will be visible in the final message.\n"original" in this case means "as passed to spamassassin", eg. if 'scanoriginal' is set to 0 above this will forward the\nmessage as retreived from previous plugins """,
            },

            'problemaction': {
                'default': 'DEFER',
                'description': "action if there is a problem (DUNNO, DEFER)",
            },

            'rejectmessage': {
                'default': 'message identified as spam',
                'description': "reject message template if running in pre-queue mode",
            },

            'mynetworks':{
                'default':'',
                'description':"a regular expression defining IP ranges of your internal networks, used to detect external ips",
            },

            'template_blockedvirus_sender': {
                'default': '/etc/fuglu/templates/blockedvirus_sender.tmpl',
                'description': 'Mail template for the bounce to inform sender about blocked virus',
            },

            'template_blockedvirus_recipient': {
                'default': '/etc/fuglu/templates/blockedvirus_recipient.tmpl',
                'description': 'Mail template for the bounce to inform sender about blocked virus',
            },

            'quarantine_account': {
                'default': 'QUARANTINEACCOUNT',
                'description': 'Recipient where is sent a copy of infected email',
            },

            'quarantine_bounce': {
                'default': 'no',
                'description': 'Bounce sender and recipient of virus email ( yes = enable , no = disabled )',
            }
        }


    def __str__(self):
        return "RSpamd"



    def lint(self):
        allok = self.check_config()
        allok = allok and self._lint_gtube()
        return allok

    def _lint_gtube(self):
        tmpfile = tempfile.mkstemp()[1]
        with open(tmpfile, 'w') as fh:
            fh.write(GTUBE)

        suspect = Suspect('oli@unittests.fuglu.org', 'oli@unittests.fuglu.org', tmpfile)
        data = self.rspamd_json(suspect,"test@example.com")

        os.remove(tmpfile)
        print(data)
        return (None not in data)

    def decode_email(self, suspect, msg_str):
        #self.logger.error('[%s] Content is %s' % (suspect.id,msg_str))
        p = Parser()
        message = p.parsestr(msg_str)
        decoded_message = ''
        for part in message.walk():
            charset = part.get_content_charset()
            part_str = part.get_payload(decode=1)
            self.logger.error('[%s] Part is %s' % (suspect.id,str(part_str)))
            if part.get_content_type() == 'text/plain':
                #part_str = part.get_payload(decode=1)
                decoded_message += part_str.decode(charset)
        return decoded_message

    def rspamd_json(self, suspect, recipient):
        # https://rspamd.com/doc/architecture/protocol.html

        host = self.config.get(self.section, 'host')
        port = self.config.getint(self.section, 'port')
        timeout = self.config.getint(self.section, 'timeout')
        if self.config.getboolean(self.section, 'scanoriginal'):
            content = suspect.get_original_source()
        else:
            content = suspect.get_source()

        mynetworksre=None
        mynetworksconfig = self.config.get(self.section,'mynetworks') or None
        if mynetworksconfig is not None:
            mynetworksre=re.compile(mynetworksconfig)
        clientinfo = suspect.client_info_from_rcvd(ignoreregex=mynetworksre)
        if clientinfo is not None:
            helo, clientip, clienthostname = clientinfo
            clientinfoheaders = {
                "IP": clientip,
                "Helo": helo,
                "Hostname": clienthostname,
            }
        else:
            clientinfoheaders = {}

        msgrep = suspect.getMessageRep()
        if msgrep:
            try:
                subj=u''.join(msgrep['Subject']).encode('utf-8').rstrip()
            except Exception as e:
                subj=str(msgrep['Subject']).rstrip()
        else:
            subj=''

        received=msgrep['Received']
        headers = {
            "Queue-Id": suspect.id,
            "Rcpt": recipient,
            "Subject": subj,
            "Json": "yes", # enforce json reply even if not set in rspamd config
        }

        #if empty from not add it to email ( use mime recipient )
        if suspect.from_address:
            try:
                encoded_from = u''.join(suspect.from_address).encode('utf-8')
            except Exception as e:
                encoded_from = suspect.from_address
            headers.update({ "From": encoded_from })

        #if there is authenticad user inside received ( smtpd_sasl_authenticated_header = yes )
        if "Authenticated sender:" in str(received) :
            m = re.search('(?<=\(Authenticated sender: )(.+)\)', received)
            authenticated_user=str(m.group()).replace(")","")
            headers.update({ "User": authenticated_user })
        else:
            authenticated_user=None

        headers.update(clientinfoheaders)

        try:
            #try to detect encoding, if not use beatifulsoup
            #test = self.decode_email(suspect, content)
            content.decode('utf-8')
        except UnicodeError:
            self.logger.error('[%s] No decode for the content, continue with no decoding' % suspect.id)

        try:
            conn = HTTPConnection(host, port, timeout=timeout)
            conn.request("POST", "/checkv2", content, headers)
            response = conn.getresponse()
            conn.close()
            jsondata = response.read()
            if isinstance(jsondata, bytes): # python3
                jsondata = jsondata.decode('utf-8')
            reply = json.loads(jsondata)
        except Exception as e:
            self.logger.error('[%s] Failed to get rspamd response for %s' % (suspect.id, str(e)))
            self.logger.error('[%s] Try to convert the content with BeautifulSoup and re connect to rspamd' % suspect.id)
            try:
                content = str(BeautifulSoup((''.join(content))))
                conn = HTTPConnection(host, port, timeout=timeout)
                conn.request("POST", "/checkv2", content, headers)
                response = conn.getresponse()
                conn.close()
                jsondata = response.read()
                if isinstance(jsondata, bytes):
                    jsondata = jsondata.decode('utf-8')
                reply = json.loads(jsondata)
            except Exception as e:
                reply = None
                self.logger.error('[%s] Failed to get rspamd response for %s' % (suspect.id, str(e)))

        #self.logger.error('[%s] All response from rspamd %s' % (suspect.id, reply))

        if reply is not None:

            if 'symbols' in reply and 'IP_MYNETWORK_ZIMBRA' in reply['symbols']:
                symbols = ["IP_MYNETWORK_ZIMBRA"]
                #clear probable memory leak
                del reply, jsondata, headers, msgrep
                return None, False, None, symbols, None, None, None, None, None

            if 'milter' in reply :
                if 'messages' in reply :
                    replysmtp = reply['messages']
                replydata = reply['symbols']
                replyheader = reply['milter']["add_headers"]
                required_score = reply['required_score']
                score = round(reply['score'],1)
                server = replyheader['X-Rspamd-Server']
            else:
                replydata = None

            rspamd_action="no action"

            if replydata:
                #isspam = 'X-Spam-Flag' in replyheader and replyheader['X-Spam-Flag'] == "YES"
                #self.logger.info('%s this rspamd action %s' % (suspect.id, json.dumps(reply, ensure_ascii=False)))
                isspam = False
                isvirus = None
                if 'action' in reply:
                    if reply['action'] == 'reject':
                        if replysmtp is not None:
                            #self.logger.info('AAAAAA %s -- %s ' % (suspect.id, replysmtp))
                            if 'smtp_message' in replysmtp and ( 'virus found' in replysmtp['smtp_message'] or 'oletools' in replysmtp['smtp_message'] ):
                                rspamd_action = reply['action']
                                isvirus = replysmtp['smtp_message']
                            else:
                                rspamd_action = reply['action']
                                isspam = True
                        else:
                            rspamd_action = reply['action']
                            isspam = True
                    if reply['action'] == 'rewrite subject' or reply['action'] == 'add header' :
                        rspamd_action = reply['action']
                        isspam = True
                    if reply['action'] == 'soft reject' or reply['action'] == 'greylist':
                        self.logger.error('%s this rspamd action %s IS NOT SUPPORTED!' % (suspect.id, reply['action']))

                #self.logger.debug('%s rspamd action for user %s is: %s' % (suspect.id,recipient , reply['action']))
                report, symbols = self._gen_report(replydata)

                #clear probable memory leak
                del replydata, replyheader, reply, jsondata, headers, msgrep

                return isvirus, isspam, report, symbols, rspamd_action, server, required_score, score, authenticated_user

            else:
                if reply is not None:
                    if authenticated_user :
                        #clear probable memory leak
                        del reply, headers, msgrep
                        isspam = False
                        #self.logger.info('[%s] Rspamd has disable result for authenticated user %s' % (suspect.id,authenticated_user))
                        #Auth user possibility to have no scan info ( personalized from rspamd config )
                        return None, False, None, None, None, None, None, None, authenticated_user

        #clear probable memory leak
        del headers, msgrep
        #Super error!
        return None, None, None, None, None, None, None, None, None
        #self.logger.info("[%s] Problem with rspamd plugin" % suspect.id)
        #self._problemcode()

    def _gen_report(self, reply):
        report = ""
        symbols = []
        keys = reply.keys()
        for key in keys:
            score = 0
            name = 'UNKNOWN'
            options = ''
            if key.isupper():
                symbol = reply[key]
                if 'score' in symbol:
                    score = symbol['score']
                if 'name' in symbol:
                    name = symbol['name']
                if 'options' in symbol:
                    options = " ".join(symbol['options'])

                line = u'%s(%s) [%s], ' % (name, score, options)
                report=report+line
                symbols.append(name)

        return report[:-2], symbols

    def examine(self, suspect):
        if suspect.get_tag('RSpamd.skip') is True:
            self.logger.debug('%s Skipping RSpamd Plugin (requested by previous plugin)' % suspect.id)
            suspect.set_tag('RSpamd.skipreason', 'requested by previous plugin')
            return DUNNO

        runtimeconfig = DBConfig(self.config, suspect)

        #multirecipient support
        #define variable
        action = None
        message = None
        deleted = []
        final_result = {}
        dictionary = {}
        myhash = None

        for recipient in suspect.recipients:

            isvirus, isspam, report, symbols, rspamd_action, server, required_score, score, authenticated_user = self.rspamd_json(suspect,recipient)

            if isvirus :
                #if virus found:
                #- send bounce to sender
                #- send bounce to all recipients
                #- change recipients with the quarantine account
                #- send original email to quaratine account
                quarantine_bounce = self.config.get(self.section, 'quarantine_bounce')
                if quarantine_bounce == "yes" :
                    self._bounce_virus_sender(suspect,isvirus)
                    self._bounce_virus_recipient(suspect,isvirus)
                quarantine_account = self.config.get(self.section, 'quarantine_account')
                suspect.recipients = [quarantine_account,]
                final_recipient = (quarantine_account,)
                suspect.multirecipient_result[final_recipient] = { 'analyzed' : 'yes' }
                suspect.multirecipient_result[final_recipient].update({ 'virus': 'yes' })
                suspect.multirecipient_result[final_recipient].update({ 'headers': {'X-Rspamd-Virus-Found':'YES'} })
                suspect.multirecipient_result[final_recipient]['headers'].update({ 'X-Rspamd-QueueID': suspect.id })
                suspect.multirecipient_result[final_recipient].update({ 'Subject-rewrite': 'No' })
                self.logger.info("[%s] Virus found, send email to quarantine account %s" % (suspect.id,quarantine_account))
                break
            elif rspamd_action == "reject" :
                isspam = True

            if isspam is None and authenticated_user is None:
                self.logger.info('[%s] defer by rspamd plugin' % suspect.id)
                return self._problemcode()
                continue
            final_result = { 'isspam':isspam, 'symbols':symbols, 'rspamd_action':rspamd_action, 'score':score }
            myhash=hashlib.sha256(repr(sorted(final_result.items()))).hexdigest()
            self.logger.debug("%s The user %s rspamd header has hash - %s" % (suspect.id,recipient,myhash))
            if myhash in dictionary:
                dictionary[myhash]['recipient'].append(recipient)
            else:
                dictionary[myhash] = { 'static_value' : { 'isspam':isspam, 'report':report, 'symbols':symbols, 'rspamd_action':rspamd_action, 'server':server, 'required_score':required_score, 'score':score, 'authenticated_user':authenticated_user  }, 'recipient': [ recipient ] }

        for key, value in dictionary.items():

	        #self.logger.info("[%s] final value spam : %s,authenticated user: %s, symbol: %s" % (suspect.id,dictionary[key]['static_value']['isspam'],authenticated_user,symbols))
            if dictionary[key]['static_value']['isspam'] is None and authenticated_user is None and not 'IP_MYNETWORK_ZIMBRA' in symbols:
                self.logger.info('[%s] defer by rspamd plugin' % suspect.id)
                return self._problemcode()
            elif dictionary[key]['static_value']['isspam']:
                #self.logger.debug('%s Message is spam' % suspect.id)
                suspect.tags['spam']['RSpamd'] = dictionary[key]['static_value']['isspam']
            #else:
                #self.logger.debug('%s Message is not spam' % suspect.id)

            final_recipient = ()

            #self.logger.info("[%s] %s %s %s" % (suspect.id,dictionary[key]['static_value']['score'],dictionary[key]['static_value']['isspam'],dictionary[key]['static_value']['rspamd_action']) )
            if dictionary[key]['static_value']['score'] and dictionary[key]['static_value']['isspam'] and dictionary[key]['static_value']['rspamd_action'] == "reject":

                for single_recipient in dictionary[key]['recipient']:
                    final_recipient = final_recipient + (single_recipient,)

                suspect.multirecipient_result[final_recipient] = { 'analyzed' : 'yes' }

                if action is None:
                    self.logger.info("[%s] 1 Recipient %s discarded for high score spam" % (suspect.id,dictionary[key]['recipient']))
                    suspect.multirecipient_result[final_recipient].update({ 'deleted' : 'yes' })
                    action = DELETE
                    message = self.config.get(self.section, 'rejectmessage')
                else :
                    self.logger.info("[%s] 2 Recipient %s discarded for high score spam" % (suspect.id,dictionary[key]['recipient']))
                    suspect.multirecipient_result[final_recipient].update({ 'deleted' : 'yes' })

		#self.logger.info(" %s " % suspect.multirecipient_result)
                #continue
            else :
                if action == DELETE :
                    message = "1 or more recipient discarded"
                action = DUNNO

            forwardoriginal = self.config.getboolean(self.section, 'forwardoriginal')
            final_recipient = ()

            if not forwardoriginal and (dictionary[key]['static_value']['symbols'] or authenticated_user):

                for single_recipient in dictionary[key]['recipient']:
                    final_recipient = final_recipient + (single_recipient,)

                #self.logger.info("%s final result %s" % (suspect.id,final_recipient) )
                if final_recipient not in suspect.multirecipient_result :
                    suspect.multirecipient_result[final_recipient] = { 'analyzed' : 'yes' }

                #self.logger.info("%s final result %s" % (suspect.id,suspect.multirecipient_result[final_recipient]) )
                if dictionary[key]['static_value']['isspam']:
                    suspect.multirecipient_result[final_recipient].update({ 'headers': {'X-Spam-Flag':'YES'} })
                else:
                    suspect.multirecipient_result[final_recipient].update({ 'headers': {} })

                #insert fuglu suspect.id inside the email header (simple retrieve into log)
                suspect.multirecipient_result[final_recipient]['headers'].update({ 'X-Rspamd-QueueID': suspect.id })

                #give the rule to rewrite
                if dictionary[key]['static_value']['rspamd_action'] == "rewrite subject":
                    suspect.multirecipient_result[final_recipient].update({ 'Subject-rewrite': 'Yes' })
                    suspect.multirecipient_result[final_recipient].update({ 'Subject-Spam-Tag': '***_SPAM_***' })
                else:
                    suspect.multirecipient_result[final_recipient].update({ 'Subject-rewrite': 'No' })

                if dictionary[key]['static_value']['report']:
                    suspect.multirecipient_result[final_recipient]['headers'].update({ 'X-Rspamd-Result':dictionary[key]['static_value']['report'] })
                    suspect.multirecipient_result[final_recipient]['headers'].update({ 'X-Rspamd-Server':dictionary[key]['static_value']['server'] })
                    suspect.multirecipient_result[final_recipient]['headers'].update({ 'X-Rspamd-Score':str(dictionary[key]['static_value']['score'])+"/"+str(dictionary[key]['static_value']['required_score']) })

        #self.logger.info(" %s ",suspect.multirecipient_result)
        #clear probable memory leak
        del isvirus,isspam, report, symbols, rspamd_action, server, required_score, score, authenticated_user, dictionary, myhash, final_result
        return action, message

    def _bounce_virus_sender(self,suspect,blockinfo):
        self.blockedvirustemplate_sender = self.config.get(
            self.section, 'template_blockedvirus_sender')
        bounce = Bounce(self.config)
        bounce.send_template_file(suspect.from_address, self.blockedvirustemplate_sender, suspect, dict(blockinfo=blockinfo,suspectid=suspect.id))
        del bounce

    def _bounce_virus_recipient(self,suspect,blockinfo):
        self.blockedvirustemplate_recipient = self.config.get(
            self.section, 'template_blockedvirus_recipient')
        bounce = Bounce(self.config)
        for recipient in suspect.recipients:
            bounce = Bounce(self.config)
            bounce.send_template_file(recipient, self.blockedvirustemplate_recipient, suspect, dict(blockinfo=blockinfo,suspectid=suspect.id))
        del bounce

    def _problemcode(self):
        retcode = string_to_actioncode(self.config.get(self.section, 'problemaction'), self.config)
        if retcode is not None:
            return retcode
        else:
            # in case of invalid problem action
            return DEFER
