-- package for dns resultion by Manuel
package.path = package.path .. ';/etc/powerdns/?.lua'
local Resolver = require "dns.resolver"

local qtt = {
      [1]     = "A",
      [38]    = "A6",
      [28]    = "AAAA",
      [65400] = "ADDR",
      [18]    = "AFSDB",
      [65401] = "ALIAS",
      [255]   = "ANY",
      [252]   = "AXFR",
      [257]   = "CAA",
      [60]    = "CDNSKEY",
      [59]    = "CDS",
      [37]    = "CERT",
      [5]     = "CNAME",
      [49]    = "DHCID",
      [32769] = "DLV",
      [39]    = "DNAME",
      [48]    = "DNSKEY",
      [43]    = "DS",
      [108]   = "EUI48",
      [109]   = "EUI64",
      [13]    = "HINFO",
      [45]    = "IPSECKEY",
      [251]   = "IXFR",
      [25]    = "KEY",
      [36]    = "KX",
      [29]    = "LOC",
      [254]   = "MAILA",
      [253]   = "MAILB",
      [14]    = "MINFO",
      [9]     = "MR",
      [15]    = "MX",
      [35]    = "NAPTR",
      [2]     = "NS",
      [47]    = "NSEC",
      [50]    = "NSEC3",
      [51]    = "NSEC3PARAM",
      [61]    = "OPENPGPKEY",
      [41]    = "OPT",
      [12]    = "PTR",
      [57]    = "RKEY",
      [17]    = "RP",
      [46]    = "RRSIG",
      [24]    = "SIG",
      [6]     = "SOA",
      [99]    = "SPF",
      [33]    = "SRV",
      [44]    = "SSHFP",
      [249]   = "TKEY",
      [52]    = "TLSA",
      [250]   = "TSIG",
      [16]    = "TXT",
      [256]   = "URI",
      [11]    = "WKS" 
}

local function estrai(sep, str)

    if not sep or sep == "" then return false end
    if not str then return false end
    local limit = 3
    
    local start, finish
    local r = {}
    local pattern = "([^" .. sep .. "]+)"
    local init = 0
    local fine = limit -1
    for i=1,fine do
      if i == 1 then
        start=init
      end
        start,finish,capture = string.find(str,pattern,start)
        start=finish+2
        if capture
        then
          table.insert(r,capture)
        else
          break
        end

    end

    table.insert(r,string.sub(str,start))
    return unpack(r)

end

-- estrai(",","PRIMO,SECONDO,TERZO,prova,gatto")

local records = {}

local override_list = io.open("/etc/powerdns/rr_override.list","r")
if override_list ~= nil 
then
  for line in io.lines("/etc/powerdns/rr_override.list") do
    if ( string.sub(line,0,1) ~= "#" ) then
      local name,qtype,content = estrai(",",line)
      name=string.lower(name) 
      if not records[name] then
        records[name]={}
      end
      if not records[name][qtype] then
        records[name][qtype]={}
      end
      table.insert(records[name][qtype],content)
    end
  end
end

function external_txt(domain, type, dq)

    local r = Resolver.new({"52.50.126.195"}, 2)
    local rec, errmsg = r:resolve(domain, type)
    if errmsg then
        return false
    else
        -- print all the founded record
        --for _, v in ipairs(rec) do
        --        print(v.name, v.type, v.class, v.content)
        --end
        dq:addAnswer(pdns.A,"127.0.0.2",2592000)
        dq.rcode=0
        dq.followupFunction="followCNAMERecords"
        return true
    end
end

function gotdomaindetails(dq)

    if( dq.udpAnswer ~= "" )
    then
        dq:addAnswer(pdns.A,"127.0.0.2",2592000)
        dq.followupFunction="followCNAMERecords"
        dq.rcode=0
        return true
    end
    dq.rcode=0
    return false
end


function preresolve ( dq )

    domain=string.lower(dq.qname:toString())
    qtype=dq.qtype

    -- whitelist postscreen
    if ( qtype == pdns.A and string.find(domain,"local.whitelist.sstorti"))
    then
        ip=string.gsub(string.gsub(domain,".local.whitelist.sstorti.",""),"(%d+).(%d+).(%d+).(%d+)", function(a,b,c,d) return d.."."..c.."."..b.."."..a end)
        dq.followupFunction="udpQueryResponse"
        dq.udpCallback="gotdomaindetails"
        dq.udpQueryDest=newCA("127.0.0.1:5555")
        dq.udpQuery = ip
        return true
    end

    -- blacklist uribl uribl.zimbraopen.it
    if ( qtype == pdns.A and string.find(domain,"uribl.zimbraopen.it"))
    then
        external_txt(domain, "TXT", dq)
        return true
    end

    -- if ( qtype == pdns.A and string.find(domain,"local.google_safe_browsing"))
    -- then
    --    pdnslog("Manuel -- SURBL "..domain)
        -- ip=string.gsub(string.gsub(domain,".local.g.",""),"(%d+).(%d+).(%d+).(%d+)", function(a,b,c,d) return d.."."..c.."."..b.."."..a end)
        --pdnslog(ip)
        --pdnslog(qtype)
        -- dq.followupFunction="udpQueryResponse"
        -- dq.udpCallback="gotdomaindetails"
        -- dq.udpQueryDest=newCA("127.0.0.1:5556")
        -- dq.udpQuery = ip
        -- return true;
    -- end

    if records[domain] then
      if records[domain][ qtt[qtype] ] or records[domain][ qtt[5] ] then
        if records[domain][ qtt[qtype] ] then
          risultati = records[domain][ qtt[qtype] ]
        else
          risultati = records[domain][ qtt[5] ]
          qtype=pdns.CNAME
        end
        for i = 1, #risultati do
          dq:addAnswer(qtype, risultati[i])
        end
        dq.followupFunction="followCNAMERecords"
	dq.rcode=0
	return true
      end
    end 
    return false
end

