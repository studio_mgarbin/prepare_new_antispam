#!/bin/bash

question(){

    echo "----------------------------------------------"
    echo "$1 (y/n)"
    echo "----------------------------------------------";

    i=0
    risposta=""

    while [ $i -lt 10 ]
    do
        read -n 1 risposta
        if [[ $risposta == "y" ]]
        then
                echo ""
                break
        fi

        if [[ $risposta == "n" ]]
        then
                echo ""
                break
        fi

        echo "";
        echo "-- Will accept only y/n char --";
        echo ""

    done

}

#only DEBIAN
linux=$(lsb_release -i -s)

if [[ "$linux" != "Ubuntu" ]]
then
	echo "--- ONLY UBUNTU SERVER! ---"
	exit
fi

question "Do you want install redis/fuglu/rspamd?"

if [[ "$risposta" == "y" ]]
then
    echo "--- add redis ext repo ( we need version 4+ ) ---"
    add-apt-repository ppa:chris-lea/redis-server
    apt-get update

    echo "--- install lua + redis + python ---"
    apt-get install -y liblua5.1-0-dev:amd64 liblua5.1-0:amd64 libluajit-5.1-common libluajit-5.1-dev:amd64 liblualib50 lua-ldap:amd64 lua-ldap:amd64 redis-server lsb-release wget python-dev python-magic python2-pip python-beautifulsoup python-lzma lua-socket

    echo "--- install python rarfile 3, support for RAR3/RAR5 ---"
    cd files/python/rarfile-rarfile_3_0/ ; python setup.py install
    cd ../../../

    echo "---  install rspamd ----"
    CODENAME=$(lsb_release -c -s)
    wget "https://rspamd.com/apt-stable/pool/main/r/rspamd/rspamd_2.7-42~${CODENAME}_amd64.deb"
    dpkg -i "rspamd_2.7-42~${CODENAME}_amd64.deb"
    #wget -O- https://rspamd.com/apt-stable/gpg.key | apt-key add -
    echo "deb [arch=amd64] http://rspamd.com/apt-stable/ $CODENAME main" > /etc/apt/sources.list.d/rspamd.list
    echo "deb-src [arch=amd64] http://rspamd.com/apt-stable/ $CODENAME main" >> /etc/apt/sources.list.d/rspamd.list
    apt-get update
    #apt-get install -y rspamd=2.7-42~bionic
    apt-mark hold rspamd
    #fix broken install of rspamd
    apt-get -f install

    echo "--- change logrotate rspamd to 30 days ---"
    sed -i 's/rotate 4$/rotate 30/g' /etc/logrotate.d/rspamd

    #echo "--- create link for lua ldap module inside rspamd lib ---"
    #ln -s /usr/lib/x86_64-linux-gnu/lua/5.1/lualdap.so /usr/lib/rspamd/

    echo "--- install fuglu 0.8.0 ---"
    pip2 install fuglu==0.8.0

    #echo "--- apply change for redis tunning (sysctl/systemd) ---"
    echo never > /sys/kernel/mm/transparent_hugepage/enabled
    cp files/redis/* /etc/systemd/system/
    systemctl daemon-reload
    systemctl enable prepare-redis-hugespace.service
    #----- da verificare se con systemd è necessario specificare queste cose nello unit file ----
    echo 'vm.overcommit_memory=1' >> /etc/sysctl.conf
    echo 'net.core.somaxconn=65535' >> /etc/sysctl.conf
    sysctl -p /etc/sysctl.conf
    systemctl restart redis-server
    echo "--- PLEASE REBOOT YOUR SYSTEM! ---"
fi

question "Do you want to configure rspamd?"

if [[ "$risposta" == "y" ]]
then
    echo "--- copy custom file for rspamd ---"
    cp files/rspamd/config/*.map files/rspamd/config/zimbra.cfg.lua files/rspamd/config/rspamd.local.lua files/rspamd/config/options.inc /etc/rspamd
    cp -R files/rspamd/config/local.d files/rspamd/config/override.d /etc/rspamd
    cp files/rspamd/config/modules.d/zimbra_mynetwork.conf /etc/rspamd/modules.d/
    mkdir /usr/share/rspamd/lua /usr/share/rspamd/spamassassin
    cp files/rspamd/share/zimbra_mynetwork.lua /usr/share/rspamd/lua/
    cp files/rspamd/share/custom_sstorti_rules.cf /usr/share/rspamd/spamassassin/
    cp files/rspamd/share/oletools.lua /usr/share/rspamd/lualib/lua_scanners/
    systemctl stop rspamd

    if [[ $(lsb_release -r -s) == "18.04" ]]
    then
      echo "--- new rspamd binary ---"
      cp files/rspamd/bin/* /usr/bin/
      cp files/rspamd/lib/* /usr/lib/rspamd/
    fi

    echo "-----------------------------"
    echo "Insert redis bind ip server ( default 127.0.0.1 ):"
    read redis_server
    echo "-----------------------------"
    hostname=$(su - zimbra -c 'zmlocalconfig zimbra_server_hostname'|awk '{ print $3}')
    ldap_hostname=$(su - zimbra -c 'zmlocalconfig ldap_master_url'|awk '{ print $3}'|sed 's/ldap\:\/\///g;s/:389//g')
    ldap_bind_password=$(su - zimbra -c 'zmlocalconfig -s zimbra_ldap_password'|awk '{ print $3}')

    sed -i "s/ZIMBRA_SERVER/$hostname/g;s/LDAP_HOSTNAME/$ldap_hostname/g;s/LDAP_PASSWORD/$ldap_bind_password/g" /etc/rspamd/zimbra.cfg.lua

    if [ "$redis_server" != "" ]
    then
      sed -i "s/REDIS_SERVER/$redis_server/g" /etc/rspamd/local.d/redis.conf /etc/rspamd/override.d/statistic.conf /etc/rspamd/zimbra.cfg.lua
    else
      sed -i "s/REDIS_SERVER/127.0.0.1/g" /etc/rspamd/local.d/redis.conf /etc/rspamd/override.d/statistic.conf /etc/rspamd/zimbra.cfg.lua
    fi

    systemctl start rspamd

    # is it necessary?
    #echo "--- configure rspamd, initialize redis DB ---"
    #rspamadm configwizard
fi

question "Do you want to configure fuglu?"

if [[ "$risposta" == "y" ]]
then
    echo "--- create directory for fuglu ---"
    mkdir /var/log/fuglu
    mkdir /etc/fuglu
    echo "--- copy custom file for fuglu ---"
    cp -R files/fuglu/core/* /usr/local/lib/python2.7/dist-packages/fuglu
    cp -R files/fuglu/config/* /etc/fuglu/
    getquarantineaccount=$(/opt/zimbra/bin/zmprov gacf zimbraAmavisQuarantineAccount|awk '{ print $2 }')
    sed -i "s/QUARANTINEACCOUNT/$getquarantineaccount/g" /etc/fuglu/extra-plugins/rspamd.py
    echo "---- SYSTEMD file ----"
    cp files/fuglu/bin/fuglu.service /etc/systemd/system
    systemctl enable fuglu
fi

question "Do you want to overwrite zimbra antispam config/activate new configuration?"

if [[ "$risposta" == "y" ]]
then

    if [[ "$(/opt/zimbra/bin/zmprov -l gs `/opt/zimbra/bin/zmhostname` zimbraServiceEnabled |grep -P 'amavis|antispam')" == "" ]]
    then
        echo "--- create custom master.cf.in config in zimbra postfix conf ---"
        echo '
#FUGLU CONFIG
smtp-fuglu unix -      -       n       -       -  smtp
    -o smtp_send_xforward_command=yes
    -o smtp_tls_security_level=none

[%%zimbraLocalBindAddress%%]:10035 inet n  -       n       -       -  smtpd
    -o content_filter=
    -o local_recipient_maps=
    -o virtual_mailbox_maps=
    -o virtual_alias_maps=
    -o relay_recipient_maps=
    -o smtpd_restriction_classes=
    -o smtpd_delay_reject=no
    -o smtpd_client_restrictions=permit_mynetworks,reject
    -o smtpd_data_restrictions=
    -o smtpd_end_of_data_restrictions=
    -o smtpd_helo_restrictions=
    -o smtpd_sender_restrictions=
    -o smtpd_reject_unlisted_sender=no
    -o smtpd_relay_restrictions=
    -o smtpd_recipient_restrictions=permit_mynetworks,reject
    -o mynetworks_style=host
    -o mynetworks=127.0.0.0/8,[::1]/128
    -o strict_rfc821_envelopes=yes
    -o smtpd_error_sleep_time=0
    -o smtpd_soft_error_limit=1001
    -o smtpd_hard_error_limit=1000
    -o smtpd_client_connection_count_limit=0
    -o smtpd_client_connection_rate_limit=0
    -o receive_override_options=no_header_body_checks,no_unknown_recipient_checks,no_address_mappings
    -o local_header_rewrite_clients=
    -o syslog_name=postfix/fuglu
' >>  /opt/zimbra/common/conf/master.cf.in

        echo "--- copy custom file fuglu_originating and fuglu_foreing and custom smtpd_sender_restrictions.cf ---"
        sed -i 's/tag_as_originating\.re\%\%/tag_as_originating\.re\%\%\ncheck_sender_access regexp:\/opt\/zimbra\/conf\/fuglu_originating\.re/g' /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf
        sed -i 's/permit_tls_clientcerts/permit_tls_clientcerts\ncheck_sender_access regexp:\/opt\/zimbra\/conf\/fuglu_foreign\.re/g' /opt/zimbra/conf/zmconfigd/smtpd_sender_restrictions.cf

        cp files/postfix/fuglu* /opt/zimbra/conf/
        echo "--- copy training rspamc file ---"
        cp -R files/zimbra/rspamd /opt/zimbra/conf/

        echo "--- crontab file for training ---"
	echo "
#BAYES global
00 22 * * * /opt/zimbra/conf/rspamd/trainsa_global.sh >> /opt/zimbra/log/rspamd_train_global.log 2>&1
00 13 * * * /opt/zimbra/conf/rspamd/trainsa_global.sh >> /opt/zimbra/log/rspamd_train_global.log 2>&1

#BAYES user
*/30 * * * * /opt/zimbra/conf/rspamd/trainsa_user.sh >> /opt/zimbra/log/rspamd_train_user.log 2>&1" >> /var/spool/cron/crontabs/zimbra
        echo "--- restart amavis zimbra ---"
        su - zimbra -c "zmconfigdctl restart; zmmtactl restart"

        if [[ "$(/opt/zimbra/bin/zmprov -l gs `/opt/zimbra/bin/zmhostname` zimbraServiceEnabled |grep -P 'antivirus')" != "" ]]
        then
          echo "--- zimbra antivirus enabled, enable configuration with rspamd ----"
          sed -i 's/zmamavisdctl zmclamdctl zmfreshclamctl/zmclamdctl zmfreshclamctl/g' /opt/zimbra/bin/zmantivirusctl
          cp files/rspamd/antivirus_enable/antivirus.conf /etc/rspamd/override.d/
          systemctl restart rspamd
        fi

    else
        echo "--- ERROR : YOU MUST DISABLE AMAVIS/SPAMASSASIN TO CONFIGURE FUGLU/RSPAMD ! ---"
        echo "su - zimbra"
        echo "/opt/zimbra/bin/zmprov -l ms `/opt/zimbra/bin/zmhostname` -zimbraServiceEnabled 'amavis' -zimbraServiceEnabled 'antispam'"
        echo "zmcontrol stop; zmcontrol start"
    fi
fi

question "Do you want to enable postscreen (only if you are using powerDNS as resolver) ?"

if [[ "$risposta" == "y" ]]
then
    echo "--- copy IP white/black list file to zimbra conf ---"
    cp files/postfix/postfix_postscreen_wblist /opt/zimbra/conf/postfix_postscreen_wblist
    echo "--- initialize PTR white/black list service ---"
    cp -R files/postscreen/postscreen_whitelist /etc/
    cp files/postscreen/systemd/postscreen_whitelist.service /etc/systemd/system/
    cp -R files/powerdns/* /etc/powerdns/
    chmod 755 /etc/postscreen_whitelist/postscreen_whitelist
    systemctl restart pdns-recursor
    echo "--- start PTR white/black list service ---"
    systemctl enable postscreen_whitelist.service
    systemctl start postscreen_whitelist.service
    echo "--- copy tool for stats postscreen ---"
    cp files/postscreen/postscreen_stats.py /usr/local/sbin/
    echo "--- enable postscreen on zimbra server ---"
    su - zimbra -c 'zmprov ms `/opt/zimbra/bin/zmhostname` zimbraMtaPostscreenNonSmtpCommandEnable yes zimbraMtaPostscreenPipeliningEnable yes zimbraMtaPostscreenBareNewlineEnable yes zimbraMtaPostscreenBareNewlineAction enforce zimbraMtaPostscreenAccessList "permit_mynetworks, cidr:/opt/zimbra/conf/postfix_postscreen_wblist" zimbraMtaPostscreenDnsblAction enforce zimbraMtaPostscreenDnsblSites "local.whitelist.sstorti*-1" zimbraMtaPostscreenDnsblTTL 30d zimbraMtaPostscreenDnsblWhitelistThreshold "-1"'
    echo "--- restart mta ---"
    su - zimbra -c 'zmmtactl restart'
fi

question "Do you want to configure oletools ( macro heuristics inspection ) ?"

if [[ "$risposta" == "y" ]]
then

    echo "--- Install python 3 and libssl dev ---"
    apt-get install -y libssl-dev python3-pip

    echo "--- Upgrade pip ---"
    pip3 install --upgrade pip

    echo "--- Install oletools and python magic"
    pip3 install oletools python-magic

    echo "--- Configure oletools and olefy ---"
    cp files/oletools/* /usr/local/lib/python3.6/dist-packages/oletools/
    cp files/olefy/bin/* /usr/local/bin/
    cp files/olefy/conf/* /etc/
    cp files/olefy/olefy.service /etc/systemd/system/

    echo "--- create olefy user and tmpfs folder ---"
    useradd olefy
    mkdir /olefy_docs_scanner
    echo 'oletmp   /olefy_docs_scanner   tmpfs defaults,noexec,nodev,nosuid,size=512m,mode=750,uid=olefy,gid=olefy 0 0' >> /etc/fstab
    mount /olefy_docs_scanner

    echo "--- start olefy  ---"
    systemctl enable olefy
    systemctl start olefy

fi
